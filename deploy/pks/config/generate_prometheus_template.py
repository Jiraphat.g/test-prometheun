#!/usr/bin/python
from pybars import Compiler
import os,sys, getopt
import json,re

def removingUpperCaseCharacters(str):
    # Create a regular expression
    regex = "[A-Z]"
    return (re.sub(regex, "", str))

def removingSpecialCharacters(str):
    regex = "[^A-Za-z0-9]"
    return (re.sub(regex, "", str))

def clean_ing_path(string_path):
    res = removingUpperCaseCharacters(string_path)
    res = removingSpecialCharacters(res)
    return  res

def checkKubeconfig(kubeconfig):
    kubeconfig = open(kubeconfig, "r")
    try:
        os.popen("kubectl --kubeconfig={0} config view -o json".format(kubeconfig)).read()
    except:
        print('error')
        
def bypass_prom_template(kubeconfigfile,prometheusfile):
    compiler = Compiler()
    prometheus_reader = open(prometheusfile, "r")
    source = prometheus_reader.read()
    template = compiler.compile(source)

    multiple_kube = kubeconfigfile.split(",")
    urls_res = []
    for config in multiple_kube:

        ing_json = os.popen("kubectl --kubeconfig={0} get ing -n kong -o json".format(config)).read()
        ings = json.loads(ing_json)
        ings = ings["items"]

        for ing in ings:
            vals = {}
            host = ing["spec"]["rules"][0]["host"]
            path = ing["spec"]["rules"][0]["http"]["paths"][0]["path"]
            vals["url"] = "https://"+host+"/"+clean_ing_path(path)+"/swagger/index.html"
            urls_res.append(vals)

    def _list(this, options, items):
        result = []
        for thing in items:
            result.append(u'        - ')
            result.extend(options['fn'](thing))
            result.append(u'\n')
        return result
    helpers = {'list': _list}
    output = template({'services': urls_res}, helpers=helpers)
    print(output)
    file = open(prometheusfile,"w")
    file.write(output)
    file.close()

def main(argv):
    inputfile = ''
    outputfile = ''
    help_string = """
    Template Command
      generate_prometheus_template.py -k <kubeconfigfile> -p <prometheusfile>
    Example Command 
      python generate_prometheus_template.py -k $KUBE_CONFIG_DEV -p prometheus.yaml
    Options 
     -k | --kubeconfig= : select kubeconfig file, you can add more file separate by ","
     -p | --promtfile= : select prometheus yaml and need add  {{#list services}}{{url}}{{/list}}
    """
    try:
        opts, args = getopt.getopt(argv, "hk:p:", ["kubeconfig=", "promtfile="])
    except getopt.GetoptError:
        print(help_string)
        sys.exit(2)

    for opt, arg in opts:

        if opt == '-h':
            print(help_string)
            sys.exit()
        elif opt in ("-k", "--kubeconfig"):
            inputfile = arg
        elif opt in ("-p", "--promtfile"):
            outputfile = arg
    bypass_prom_template(kubeconfigfile=inputfile,prometheusfile=outputfile)

if __name__ == "__main__":
   main(sys.argv[1:])

