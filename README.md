# Prometheus

## Architecture Overview.
This diagram illustrates the architecture of Prometheus on runner:
```plantuml
rectangle WebUI 
rectangle WebAPI
rectangle Database
WebUI ->  WebAPI
WebAPI -> Database
@enduml
```

```plantuml
@startuml

actor "Developer, Vender , IT" as Developer
actor "User" as User

cloud azure {
    rectangle application_proxy
    rectangle azure_storage_account
}

rectangle azure_app_proxy_connector

rectangle "docker-compose (gitlab-runner) nutanix (192.100.10.105-10.110.172.151) / pks (10.10.10.111) " as docker_compose_nutanix_linux {
    rectangle "grafana:3000" as grafana {
        rectangle grafana_datasource
        rectangle grafrana_dashboard
        rectangle grafrana_renderer
        rectangle grafrana_alert
    }
    rectangle prometheus   {
        rectangle prometheus_job
        rectangle "prometheus_web:9090" as prometheus_web
    }
    rectangle "influxdb:8086" as influxdb   
    rectangle redis 
    rectangle "cadvisor:8080" as cadvisor
    rectangle "node_exporter:9100" as node_exporter 
    rectangle "snmp_exporter" #pink
    rectangle "oracledb_exporter:9161" as oracledb_exporter
    rectangle "blackbox_exporter" #pink
        
    redis -up-> cadvisor
    cadvisor -up-> prometheus_job
    node_exporter -up-> prometheus_job

    snmp_exporter -up-> prometheus_job
    oracledb_exporter -up-> prometheus_job
    blackbox_exporter -down-> prometheus_job

    influxdb -up-> grafana_datasource
    prometheus_job -up-> prometheus_web
    prometheus_web -up-> grafana_datasource
    grafana_datasource -up-> grafrana_dashboard
    grafrana_dashboard -> grafrana_alert 
    grafrana_alert -> grafrana_renderer
    grafrana_renderer -> azure_storage_account
    grafrana_dashboard -up-> azure_app_proxy_connector
    azure_app_proxy_connector -up-> application_proxy

    Developer -up-> application_proxy
    User -up-> grafrana_dashboard

}
cloud alert_service {
    rectangle EMAIL
    rectangle LINE
    rectangle MS_TEAM
     
    grafrana_alert -> LINE 
}
grafrana_alert -up-> alert_service

cloud external_url {
    rectangle webA
    rectangle webB
    rectangle webC
}
external_url <-> blackbox_exporter

rectangle "nutanix_exporter:9405" as nutanix_exporter
nutanix_exporter -> prometheus_job

cloud "nutanix prism:9440" as prism {
}
prism -up-> nutanix_exporter

rectangle "windows_exporter:9182" as windows_exporter {
    rectangle IIS_ambit
    rectangle IIS_in_house
    rectangle AD
    rectangle MSSQL
}
windows_exporter <-up-> prometheus_job

rectangle "aix server" as aix  {
    rectangle "oracle:1521" as oracle
    rectangle "nimon" 

    nimon -up-> influxdb
    oracle <-up-> oracledb_exporter
}

cloud "Network Device" as Network_Device  {
    rectangle "LoadBalance" #pink
    rectangle "Router" #pink
    rectangle "Switch" #pink
    rectangle "Firewall" #pink
}
Network_Device -up-> snmp_exporter

@enduml
```

## Install Peometheus 

follw on this link

https://prometheus.io/docs/guides/cadvisor/

# For Nutanix on Docker

https://github.com/claranet/nutanix-exporter.git

```
IMAGE_TAG="local/nutanix_exporter"
DOCKER_PATH="./nutanix-exporter"
docker build -t $IMAGE_TAG $DOCKER_PATH
```

# For Nutanix

Download & Install Nutanix Exploer on this link

https://next.nutanix.com/how-it-works-22/monitoring-part-3-4-metrics-and-prometheus-and-grafana-38466

Addition Config on CentOS 

https://www.howtoforge.com/tutorial/how-to-install-prometheus-and-node-exporter-on-centos-7/

```
firewall-cmd --add-port=9095/tcp --permanent
firewall-cmd --reload
```

Edit File /etc/systemd/system/nutanix_export.service 

    [Unit]
    Description=Prometheus Nutanix Exporter
    Documentation=https://next.nutanix.com/how-it-works-22/monitoring-part-3-4-metrics-and-prometheus-and-grafana-38466
    After=network.target
    Wants=network.target

    [Service]
    User=root
    Group=root
    Restart=on-failure

    #Change this line if you download the 
    #Prometheus on different path user
    ExecStart=/root/nutanix-exporter/nutanix-exporter_linux_amd64  -nutanix.conf /root/nutanix-exporter/Nutanixconf.yml
    Type=Simple

    [Install]
    WantedBy=multi-user.target

Restart service on runner

```
systemctl start nutanix_export
systemctl status nutanix_export
```

## For Windows

install windows_exporter on target server and testing connection at http://localhost:9182/metrics

https://github.com/prometheus-community/windows_exporter

```
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

Invoke-WebRequest -Uri https://github.com/prometheus-community/windows_exporter/releases/download/v0.15.0/windows_exporter-0.15.0-386.msi -OutFile prometheus_windows_exporter.msi

# for standard server
# msiexec /i prometheus_windows_exporter.msi ENABLED_COLLECTORS="cpu,cs,logical_disk,memory,net,os,service,system,tcp"

# for iis server
msiexec /i prometheus_windows_exporter.msi ENABLED_COLLECTORS="cpu,cs,logical_disk,memory,net,os,service,system,tcp,iis"

# for mssql server
msiexec /i prometheus_windows_exporter.msi ENABLED_COLLECTORS="cpu,cs,logical_disk,memory,net,os,service,system,tcp,mssql"

# for ad+dns+dhcp server
msiexec /i prometheus_windows_exporter.msi ENABLED_COLLECTORS="cpu,cs,logical_disk,memory,net,os,service,system,tcp,ad,dns,dhcp"

# verify install
Get-Service | Where-Object {$_.Name -eq "windows_exporter"} 
netstat -a | Select-String "9182"
Invoke-WebRequest -UseBasicParsing -Uri http://localhost:9182/metrics
```

```
choco install prometheus-wmi-exporter.install
```

## For Kube

https://next.nutanix.com/karbon-kubernetes-orchestration-30/deploying-prometheus-on-nutanix-karbon-31245

## For AIX

https://www.ibm.com/support/pages/nimon-working-prometheus

# Telegraf

# For SNMP

https://github.com/prometheus/snmp_exporter


# for Oracle

https://github.com/iamseth/oracledb_exporter

```
docker run -d --name oracledb_exporter --link=oracle -p 9161:9161 
-e DATA_SOURCE_NAME=system/oracle@oracle/xe 
iamseth/oracledb_exporter:alpine
```
# for blackbox_exporter

https://github.com/prometheus/blackbox_exporter
